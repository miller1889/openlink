# RECEIVE_BOOT_COMPLETED Permission
In this document I try to explain why Easy Open Link requires the 
[RECEIVE_BOOT_COMPLETED permission](https://developer.android.com/reference/android/Manifest.permission.html#RECEIVE_BOOT_COMPLETED).

The text has become longer than I wished it would be and it becomes a little bit
technical in the end, but I hope it explains:

* why the permission is required
* that I really, really thought about it before requesting the permission
* that the app does not waste any resources in the background

## The Good
Prior to version 1.5 the app did not require any permissions. 

## The Bad
This changed when a new feature was introduced which lets the user open HTTP and
HTTPS links in incognito mode if this is supported by any installed browser. 
(The initial [request](https://gitlab.com/marc.nause/openlink/issues/6) for this
feature is archived.)

In order to decide if the option to let the user open a link in incognito mode
shall be presented in the share menu or not, the app has to know if a browser
which supports opening links in incognito mode is installed before the menu
is displayed by the Android system.

Browsers which support opening links in incognito mode are at the moment of 
writing this text:

* Firefox 55+
* Fennec 55+
* Jelly
 
Easy Open Link does not run in background, it is started when the user taps the
"open link"- or "open link incognito"-option in the share menu. For this reason
Easy Open Link can not check if a browser which supports opening link in 
incognito mode is installed or not continuously. Displaying the "open link 
incognito" option without a suitable browser would lead to situations where the 
user taps on an option in the share menu which can not be executed as expected. 
Bad user experience, nobody wants that.

## The Ugly
To be able to know if a suitable browser is installed, Easy Open Link checks 
every time it is started anyway. If it finds a suitable browser it will tell
the Android system to include the "open link incognito" entry in the share menu
the next time it is accessed by a user.

That's better, but still not good. If there is only one suitable browser 
installed when Easy Open Link checks and it is uninstalled later, the user
will still see the entry, but will not be able to use incognito mode. At this
point Easy Open Link could either open the link anyway (but not in incognito
mode) or refuse to open the link. Both alternatives are not what I consider good
design.

For this reason Easy Open Link needs to tell the Android system that it wants
to be informed every time new apps are installed or uninstalled. Up until 
Android 7 (Nougat) it was possible to add this request to a small file called
the manifest which is read by the Android system when the app is installed. The 
Android system would keep in mind that an app wants to be informed and would 
wake up that app if another app was installed or removed.

Starting with Android 8 (Oreo) this is not possible anymore. It is still
possible to add the request to be informed to the manifest, but the Android
system will ignore it. It will only accept the request if it comes from an app
which is currently running. (There is a good 
[video](https://youtu.be/vBjTXKpaFj8) if you are interested in the reasons why 
Google decided to make these changes.)

Since it may take a long time until Easy Open Link is started for the first 
time, it may miss lots of potential uninstalls of its beloved suitable browser. 
That's why I decided to let the Android system start Easy Open Link on startup. 
This requires the RECEIVE_BOOT_COMPLETED permission. Easy Open Link will do a 
quick check if a suitable browser is installed and will enable or disable the 
"open link  incognito" option accordingly. It will also tell the Android system 
that it wishes to be informed every time an app is installed or uninstalled. 
This should take considerably less time than a second and the Android system is 
free to remove Easy Open Link from memory after that.

If any app is installed or uninstalled, Easy Open Link will be able to check
again if conditions are met to enable or disable the "open link incognito" 
option. Again, this will take only some milliseconds and the app will not do
anything else after that. It will not waste any CPU cycles and the Android
system is free to remove it from RAM.

## Conclusion
I hope that this text has made clear that even though the permission requested
by Easy Open Link can be abused to do harm to overall performance of the system 
and battery life if used to start lots of background work, the work done by
Easy Open Link is kept down to the absolute minimum and should not affect the
user experience in any negative way.

If you like to take a look at the code which is executed right after the
start of the system and every time an app is installed or removed, take a look
at the [BroadcastReceiver](https://gitlab.com/marc.nause/openlink/blob/e81bdf09af691e423da378db1ddbea78e1fa6017/app/src/main/java/de/audioattack/openlink/conf/BroadcastReceiver.java)
class as a start.
